import csv
import serial

# Ouvrir la connexion série avec l'Arduino
ser = serial.Serial('/dev/ttyACM0', 9600)

def find_badge(badge_id):
    with open('/home/admin/Documents/projet/bdd.csv', 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            if row[0] == badge_id:
                return row[0]
    return None

while True:
    if ser.in_waiting > 0:
        line = ser.readline().decode('utf-8').strip()
#print(f"Reçu de l'Arduino: {line}")
        if line.startswith("Card UID"):
            badge_id = line.split(":")[1].strip()
            name = find_badge(badge_id)
            if name is not None:
                print(f"Badge trouvé {badge_id}")
                ser.write(f"1\n".encode('utf-8'))  # Envoyer le nom à l'Arduino
            else:
                print(f"Badge non trouvé {badge_id}")
                ser.write("0\n".encode('utf-8'))  # Envoyer une erreur à l'Arduino//

            
