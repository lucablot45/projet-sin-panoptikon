byte Buzzer = 3;  // Buzzer port
byte Led = 5;   // Led indication intrusion port
byte GreenLed = 6; // Led Verte port 
byte RedLed = 7; // Led rouge port 


#define __main__
void setup() {
    Serial.begin(9600);     // Initialisation de la communication série
    mfrc522_init();          // Initialisation du module MFRC522
    mouvement_init();        // Initialisation du capteur de mouvement
    pinMode(Buzzer, OUTPUT); // Configuration du Buzzer en sortie 
    pinMode(Led, OUTPUT);    // Configuration de la LED en sortie
    pinMode(GreenLed, OUTPUT);  // Configuration de la LED en sortie
    pinMode(RedLed, OUTPUT); // Configuration de la LED en sortie
    digitalWrite(RedLed, HIGH); // Led rouge initialement allumé 
   
}

void loop() {
    int valeur = readmfrc522(); // Lecture de la valeur RFID
    if ( valeur == -1 ) { // Si valeur du capteur = -1
     Serial.println("Pb capteur");
    }
      else {
      int etat = mouvement();     // Lecture de l'état du capteur de mouvement
      int valeur = readmfrc522(); // Lecture de la valeur RFID
      //Serial.println(String(valeur)+"..."+String(etat));

     if (valeur == 0 && etat == 1) { // Si aucun badge n'est badger et qu'un mouvement est detecter alors, 
     // Serial.println ("Intrusion");   // Affichage "Intrusion" si le capteur de mouvement est activé et la valeur RFID est 0
      digitalWrite(Buzzer, HIGH);    // Allumage du buzzer
      digitalWrite(Led, HIGH);       // Allumage de la Led
      delay(10000);                   // Attendre 10 secondes     
      digitalWrite(Buzzer, LOW);     // Extinction du buzzer
      digitalWrite(Led, LOW);        // Extinction de la Led
} 
      else if (valeur == 1 && etat == 0) { // Si le bon badge est badger et un mouvement est detecter alors,  
          //Serial.println ("RAS");         // Affichage "RAS" si le capteur de mouvement est activé et la valeur RFID est 1
          digitalWrite( RedLed , LOW ); // Led rouge éteinte
          digitalWrite( GreenLed , HIGH); // Led verte allumer 
          delay(10000); // Délais de 10 s 
          digitalWrite( GreenLed , LOW); // Led verte éteinte
          digitalWrite( RedLed , HIGH ); // Led rouge allumer 
        }  
        else if (valeur == 1 && etat == 1) { // Si le bon badge est badger et un mouvement est detecter alors, 
          //Serial.println ("RAS");         // Affichage "RAS" si le capteur de mouvement est activé et la valeur RFID est 1
          digitalWrite( RedLed , LOW ); // Led rouge éteinte
          digitalWrite( GreenLed , HIGH); // Led verte allumer 
          delay(10000); // Délais de 10 s 
          digitalWrite( GreenLed , LOW); // Led verte éteinte
          digitalWrite( RedLed , HIGH ); // Led rouge allumer
        }  
        else          
          return ("rien");         // Affichage "rien" si le capteur de mouvement est activé et la valeur RFID est 1          
      }      
    
}

