byte Pin = 2;   // Intitialisation broche capteur  

void mouvement_init(void) {  
  pinMode(Pin, INPUT);    // capteur en entrée 
}

byte mouvement(void) {  
    byte etat = digitalRead(Pin); // Lire le capteur de mouvement 
    if (etat == HIGH) // Si mouvement, etat haut 
        return 1; 
    else if (etat == LOW) // Si pas de mouvement, etat bas 
        return 0;
}

#ifndef __main__
void setup() {
  Serial.begin(9600); // Initialisation de la communication série
  mouvement_init(); // Initialisation des broches
}

void loop() {
  Serial.println(mouvement()); // Exécution de la fonction principale
  delay(2000); // délais de 2000 ms 
}
#endif
