#include <SPI.h>
#include <MFRC522.h>
#include <Wire.h>
#include <rgb_lcd.h>

#define RST_PIN 9   // Pin de réinitialisation (RST)
#define SS_PIN 10   // Pin de sélection (SS)

MFRC522 mfrc522(SS_PIN, RST_PIN);  // Création d'une instance de MFRC522
rgb_lcd lcd;

void mfrc522_init() {
  while (!Serial);  // Attendre que le port série soit ouvert
  SPI.begin();      // Initialisation du bus SPI
  mfrc522.PCD_Init();  // Initialisation du module MFRC522
  mfrc522.PCD_DumpVersionToSerial();   // Afficher les détails du module MFRC522
  Serial.println(F("Scan PICC to see UID, SAK, type, and data blocks..."));
  lcd.begin(16, 2); // Initialiser l'écran LCD avec 16 colonnes et 2 lignes
}

int readmfrc522() {
  lcd.clear(); // Effacer le contenu actuel de l'écran LCD

  if (!mfrc522.PICC_IsNewCardPresent()) {
    return ;
  }

  // Sélectionner une carte
  if (!mfrc522.PICC_ReadCardSerial()) {
    return ;
  }

  // Afficher les informations du badge
  mfrc522.PICC_DumpToSerial(&(mfrc522.uid));

  while (!Serial.available ()) { } 
    String dataFromPi = Serial.readStringUntil('\n'); // Lire les données du port série 
    /*Serial.println("Données reçues de la Raspberry Pi : " + dataFromPi);*/
    String textToShow = dataFromPi.substring(1); // Enlever le 1er caractere 
    lcd.setCursor(0, 0); // Positionner le curseur en haut à gauche
    lcd.print(textToShow); // Afficher dataFromPi sur l'écran LCD
int dataFromPi1 = dataFromPi.toInt(); //Convertion en int 
return dataFromPi1;

}

#ifndef __main__
void setup() {
  Serial.begin(9600);  // Initialisation de la communication série avec le PC
  mfrc522_init(); // Initialisation du capteur RFID
}

void loop() {
  readmfrc522(); // Lecture des données du capteur RFID 
}
#endif

