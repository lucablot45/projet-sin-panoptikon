import csv
import serial
from datetime import datetime

# Ouvrir la connexion série avec l'Arduino
ser = serial.Serial('COM9', 9600)

def find_badge(badge_id, filename):
    with open(filename, 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            if row and row[0] == badge_id:
                return row[0], row[1]  # Retourner également le nom
    return None, None

def find_tool(tool_number, filename):
    with open(filename, 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            if row and row[0] == tool_number:
                return row[0], row[1]  # Retourner également le nom de l'outil
    return None, None

def get_tool_status(tool_name, filename):
    with open(filename, 'r') as tool_file:
        reader = csv.DictReader(tool_file)
        for row in reader:
            if row['tool_name'] == tool_name:
                return row['etat']
    return None

def update_tool_status(tool_name, new_tool_status, filename):
    with open(filename, 'r') as tool_file:
        reader = csv.reader(tool_file)
        rows = list(reader)
    with open(filename, 'w', newline='') as tool_file:
        writer = csv.writer(tool_file)
        for row in rows:
            if len(row) >= 2 and row[1] == tool_name:
                if len(row) >= 3:
                    row[2] = new_tool_status  # Mettre à jour l'état de l'outil
                else:
                    row.append(new_tool_status)  # Ajouter l'état de l'outil s'il n'existe pas
            writer.writerow(row)


def add_tool(tool_name, tool_number, filename): #apagnan quoicoubeh 
    with open(filename, 'a', newline='') as file:
        writer = csv.DictWriter(file, fieldnames=['tool_number', 'tool_name', 'etat']) #con de ron 
        writer.writerow({'tool_number': tool_number, 'tool_name': tool_name, 'etat': 'Disponible'})

def log_activity(user_name, tool_name, action, filename_logs, filename_tools): #le zgueg de michel
    with open(filename_logs, 'a', newline='') as log_file: # la conjonctivite de BLot 
        writer = csv.writer(log_file) # le gros jaloux 
        writer.writerow([datetime.now().strftime("%Y-%m-%d %H:%M:%S"), user_name, tool_name, action])    
    # Mettre à jour l'état de l'outil dans le fichier des outils
    update_tool_status(tool_name, action, filename_tools)

while True:
    if ser.in_waiting > 0:
        line = ser.readline().decode('utf-8').strip()
        #print("Données reçues de l'Arduino :", line)  # Ajout de l'instruction print
        if line.startswith("Card UID"):
            badge_id = line.split(":")[1].strip()
            badge_id, user_name = find_badge(badge_id, 'static/csv/bdd.csv')
            if user_name is not None:
                ser.write('1'.encode('utf-8') + user_name.encode('utf-8'))
                print(f"Badge trouvé {badge_id}, utilisateur {user_name}")
                # Lire le deuxième badge pour l'outil
                tool_number = None
                while tool_number is None:
                    line = ser.readline().decode('utf-8').strip()
                    if line.startswith("Card UID"):
                        tool_number = line.split(":")[1].strip()
                tool_number, tool_name = find_tool(tool_number, 'static/csv/tools.csv')
                if tool_name is not None:
                    ser.write('1'.encode('utf-8') + tool_name.encode('utf-8'))
                    print(f"Outil trouvé {tool_number}, nom de l'outil {tool_name}")
                    # Obtenir le statut actuel de l'outil dans les logs
                    tool_status = get_tool_status(tool_name, 'static/csv/tools.csv')
                    # Déterminer l'action à enregistrer en fonction de l'état actuel de l'outil
                    action = 'Pris' if tool_status == 'Disponible' else 'Disponible'
                    # Enregistrer l'action dans les logs et mettre à jour l'état de l'outil
                    log_activity(user_name, tool_name, action, 'static/csv/logs.csv', 'static/csv/tools.csv')
                else:
                    print(f"Outil non trouvé {tool_number}")
            else:
                print(f"Badge non trouvé {badge_id}")
                ser.write('0'.encode('utf-8'))  