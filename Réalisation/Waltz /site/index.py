import csv
import tempfile
import os
from flask import Flask, render_template, request, redirect, url_for, flash

app = Flask(__name__)
app.secret_key = 'test'  # N'oubliez pas de définir une clé secrète pour votre application Flask.

# Fonction pour trouver le nom associé à un badge
def find_name(badge_id, filename):
    with open(filename, 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            if row and row[0] == badge_id:
                return row[1]
    return None

# Fonction pour trouver l'outil associé à un numéro d'outil
def find_tool(tool_number, filename):
    with open(filename, 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            if row and row[0] == tool_number:
                return row[1]
    return None

# Route pour la page d'accueil
@app.route('/', methods=['GET', 'POST'])
def home():
    data = []
    with open('./static/csv/bdd.csv', 'r') as file:
        reader = csv.DictReader(file)
        for row in reader:
            if 'badge_id' in row and 'Nom' in row:
                data.append({'badge_id': row['badge_id'], 'Nom': row['Nom']})
                
    tools_data = []
    with open('./static/csv/tools.csv', 'r') as file:
        reader = csv.DictReader(file)
        for row in reader:
            if 'tool_name' in row and 'tool_number' in row:
                tools_data.append({'tool_name': row['tool_name'], 'tool_number' : row['tool_number']})

    return render_template('index.html', data=data, tools_data=tools_data)

# Route pour la page des logs
@app.route('/logs')
def logs():
    # Traitement pour afficher les logs des outils (similaire à ce que vous avez déjà)
    logs_data = []
    with open('static/csv/logs.csv', 'r', newline='', encoding='utf-8') as csvfile:  # Ajout de l'encodage et gestion des caractères non valides
        reader = csv.DictReader(csvfile)
        for row in reader:
            logs_data.append(row)
    
    # Récupération des états des outils
    tool_states = {}
    with open('static/csv/tools.csv', 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            tool_states[row['tool_name']] = row['etat']

    # Transmettre les données lues au modèle HTML
    return render_template('logs.html', logs_data=logs_data, tool_states=tool_states)


# Route pour afficher les logs des outils
@app.route('/tool_logs')
def tool_logs():
    # Traitement pour afficher les logs des outils (similaire à ce que vous avez déjà)
    tools_logs_data = []
    with open('./static/csv/tools.csv', 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            tools_logs_data.append(row)
    return render_template('logs.html', tools_data=tools_logs_data)  # Modifier tools_logs_data en tools_data

# Route pour supprimer un outil
@app.route('/delete_tool', methods=['POST'])
def delete_tool():
    # Traitement pour supprimer un outil (similaire à ce que vous avez déjà)
    tool_number_to_delete = request.form.get('tool_number')

    with open('./static/csv/tools.csv', 'r') as file, tempfile.NamedTemporaryFile('w', delete=False) as temp_file:
        reader = csv.DictReader(file)
        writer = csv.DictWriter(temp_file, fieldnames=reader.fieldnames)
        writer.writeheader()

        for row in reader:
            if row['tool_number'] != tool_number_to_delete:
                writer.writerow(row)

    os.replace(temp_file.name, './static/csv/tools.csv')
    flash('L\'outil a été supprimé avec succès !', 'danger')

    return redirect(url_for('home'))

# Route pour ajouter un outil
@app.route('/add_tool', methods=['POST'])
def add_tool():
    # Traitement pour ajouter un outil (similaire à ce que vous avez déjà)
    tool_name = request.form.get('tool_name')
    tool_number = request.form.get('tool_number')

    # Vérifier si les champs ne sont pas vides et respectent les contraintes de longueur
    if tool_name.strip() and tool_number.strip() and len(tool_number) == 11:
        with open('./static/csv/tools.csv', 'a', newline='') as file:
            writer = csv.DictWriter(file, fieldnames=['tool_number', 'tool_name', 'etat'])
            writer.writerow({'tool_number': tool_number, 'tool_name': tool_name, 'etat': 'Disponible'})
        flash('L\'outil a été ajouté avec succès !', 'success')
    else:
        flash('Vous ne devez pas dépasser 11 caractères pour les numéros et/ou laisser du vide dans un champs !', 'danger')

    return redirect(url_for('home'))

# Route pour ajouter un utilisateur
@app.route('/add_user', methods=['POST'])
def add_user():
    # Traitement pour ajouter un utilisateur
    nom = request.form.get('name')
    badge_id = request.form.get('badge')

    # Vérifier si les champs ne sont pas vides et respectent les contraintes de longueur
    if nom.strip() and badge_id.strip() and len(badge_id) == 11:
        with open('./static/csv/bdd.csv', 'a', newline='') as file:
            writer = csv.DictWriter(file, fieldnames=['badge_id', 'Nom'])
            writer.writerow({'badge_id': badge_id, 'Nom': nom})
        flash('L\'utilisateur a été ajouté avec succès !', 'success')
    else:
        flash('Vous ne devez pas dépasser 11 caractères pour les numéros et/ou laisser du vide dans un champs !', 'danger')

    return redirect(url_for('home'))

# Route pour supprimer un utilisateur
@app.route('/delete_user', methods=['POST'])
def delete_user():
    # Traitement pour supprimer un utilisateur
    badge_id_to_delete = request.form.get('badge_id')

    with open('./static/csv/bdd.csv', 'r') as file, tempfile.NamedTemporaryFile('w', delete=False) as temp_file:
        reader = csv.DictReader(file)
        writer = csv.DictWriter(temp_file, fieldnames=reader.fieldnames)
        writer.writeheader()

        for row in reader:
            if row['badge_id'] != badge_id_to_delete:
                writer.writerow(row)

    os.replace(temp_file.name, './static/csv/bdd.csv')
    flash('L\'utilisateur a été supprimé avec succès !', 'danger')

    return redirect(url_for('home'))

if __name__ == '__main__':
    app.run(debug=True)
