window.onload = function() {
    var form = document.querySelector('form');
    var refreshContent = document.querySelector('meta[http-equiv="refresh"]').getAttribute('content');

    form.addEventListener('focus', function() {
        document.querySelector('meta[http-equiv="refresh"]').removeAttribute('content');
    }, true);

    form.addEventListener('blur', function() {
        document.querySelector('meta[http-equiv="refresh"]').setAttribute('content', refreshContent);
    }, true);
};

function confirmDelete() {
    return confirm('Êtes-vous sûr de vouloir supprimer cet utilisateur ?');
}
