#include <Keypad.h>
#include <Servo.h>

const byte ROWS = 4; // Quatre lignes
const byte COLS = 3; // Trois colonnes
char keys[ROWS][COLS] = {
  {'*','0','#'},
  {'9','8','7'},
  {'6','5','4'},
  {'3','2','1'}
};
byte rowPins[ROWS] = {5, 4, 3, 2}; // Broches des lignes du clavier
byte colPins[COLS] = {8, 7, 6}; // Broches des colonnes du clavier

Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

const char correctCode[] = {'1', '2', '3', '4'};
char enteredCode[4]      = {0, 0, 0, 0}; // Stocke les touches entrées
int codeIndex            = 0; // Index actuel du code entré

Servo myServo; // Déclaration de l'objet servo
bool doorOpen = false; // Indique si la porte est ouverte ou fermée

void setup() {
  Serial.begin(9600);
  myServo.attach(9); // Attache le servo moteur à la broche 9
  closeDoor();
}

// Fonction pour réinitialiser le code entré
void resetCode() {
  codeIndex = 0;
  memset(enteredCode, 0, sizeof(enteredCode)); // Efface le tableau enteredCode
}

void openDoor() {
  Serial.println("Début ouverture");
  myServo.write(90); // Fait tourner le servo moteur à 90 degrés (position de la moitié)
  doorOpen = true; // Indique que la porte est ouverte
  Serial.println("Fin ouverture");
}

void closeDoor() {
  Serial.println("Début fermeture");
  myServo.write(0); // Fait tourner le servo moteur à 0 degré (position fermée)
  doorOpen = false; // Indique que la porte est fermée
  Serial.println("Fin fermeture");
}

void loop() {
  char key = keypad.getKey();
  
  if (key != NO_KEY) {
    Serial.println(key);
    
    enteredCode[codeIndex % 4] = key;
    codeIndex++;
    
    // Vérifie si le code entré correspond au code correct
    if (codeIndex == 4 ) {
      if ( strncmp(enteredCode, correctCode, 4) == 0) {
        // code bon
        Serial.println("Code bon");
        
        if (!doorOpen) {
            openDoor(); // Ouvre la porte si elle est fermée
          } 
        else {
            closeDoor(); // Ferme la porte si elle est ouverte
          }
        delay(100);
      } 
      else {
        // code mauvais
        Serial.println("Code incorrect");
        }
        
      resetCode(); // Réinitialise le code si le code entré est incorrect
      }  // fin de saisie
   }  // si touche appuyée
  
  delay(10); // Ajout d'un petit délai pour maintenir la connexion active
}
