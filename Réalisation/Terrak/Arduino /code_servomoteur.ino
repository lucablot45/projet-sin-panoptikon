#include <Keypad.h>
#include <Servo.h>
#include <SoftwareSerial.h>

const byte ROWS = 4;
const byte COLS = 3;
char keys[ROWS][COLS] = {
  {'*','0','#'},
  {'9','8','7'},
  {'6','5','4'},
  {'3','2','1'}
};
byte rowPins[ROWS] = {5, 4, 3, 2};
byte colPins[COLS] = {8, 7, 6};

Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

char correctCode[] = {'1', '2', '3', '4'}; // Code correct initial
char enteredCode[4] = {0, 0, 0, 0};
int codeIndex = 0;

Servo myServo;
bool doorOpen = false;

bool resetRequested = false;
bool changeCodeRequested = false;
bool codeEntryAllowed = true; // Variable pour autoriser ou non la saisie du code

SoftwareSerial BTSerial(10, 11); // RX, TX pins for HC-05 module

void setup() {
  Serial.begin(9600);
  BTSerial.begin(9600);
  myServo.attach(9);
  closeDoor();
}

void resetCode() {
  codeIndex = 0;
  memset(enteredCode, 0, sizeof(enteredCode));
  resetRequested = false;
}

void openDoor() {
  Serial.println("Début ouverture");
  myServo.write(90);
  doorOpen = true;
  Serial.println("Fin ouverture");
}

void closeDoor() {
  Serial.println("Début fermeture");
  myServo.write(0);
  doorOpen = false;
  Serial.println("Fin fermeture");
}

void handleBluetoothCommand(char command) {
  switch(command) {
    case '!': // Commande pour changer le code
      changeCodeRequested = true; // Définir une demande de changement de code
      Serial.println("Command received: Change code");
      setNewCode(); // Appeler la fonction pour définir le nouveau code
      break;
    // Ajouter d'autres cas pour d'autres commandes au besoin
    default:
      break;
  }
}

void setNewCode() {
  Serial.println("Entrez le nouveau code (4 chiffres) :");
  int i = 0;
  codeEntryAllowed = false; // Désactiver la saisie du code pendant la configuration du nouveau code
  while (i < 4) {
    if (BTSerial.available()) {
      char key = BTSerial.read();
      if (key >= '0' && key <= '9') { // Assurez-vous que le caractère reçu est un chiffre
        correctCode[i] = key;
        Serial.print(key); // Afficher le chiffre entré
        i++;
      }
    }
  }
  Serial.println("\nNouveau code enregistré !");
  codeEntryAllowed = true; // Réactiver la saisie du code après la configuration du nouveau code
  Serial.println("Changement de code terminé.");
  Serial.println("Entrez le code deux fois de suite pour confirmation");
}

void loop() {
  char key = keypad.getKey();
  
  if (key != NO_KEY && codeEntryAllowed) { // Vérifier si la saisie du code est autorisée
    Serial.println(key);
    
    if(changeCodeRequested) { // Si une demande de changement de code est en cours
      if(key == '#') {
        Serial.println("Erreur : Caractère invalide pour le changement de code.");
      } else {
        enteredCode[codeIndex % 4] = key;
        codeIndex++;
      
        if (codeIndex == 4 ) {
          changeCodeRequested = false; // Réinitialiser la demande de changement de code
          resetCode(); // Réinitialiser le code entré
        }
      }
    } else { // Si aucun changement de code n'est en cours
      if(key == '#') {
        resetRequested = true;
      } else {
        enteredCode[codeIndex % 4] = key;
        codeIndex++;
      
        if (codeIndex == 4 ) {
          if (strncmp(enteredCode, correctCode, 4) == 0) {
            Serial.println("Code bon");
            
            if (!doorOpen) {
              openDoor();
            } 
            else {
              closeDoor();
            }
            delay(100);
          } 
          else {
            Serial.println("Code incorrect");
          }
        
          resetCode();
        }
      }
    }
  }
  
  if (resetRequested) {
    resetCode();
    Serial.println("Réinitialisation terminée");
  }
  
  if (BTSerial.available()) {
    char command = BTSerial.read();
    handleBluetoothCommand(command);
  }
  
  delay(10);
}
